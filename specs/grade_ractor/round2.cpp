#include <iostream>
#include <map>
#include <string>
#include <vector>
#include <gtest/gtest.h>

#include "Rectangle.h"
#include "Button.h"
#include "List.h"
#include "Checkbox.h"
#include "Slider.h"
#include "Label.h"
#include "Window.h"

using std::map;
using std::vector;
using std::string;

TEST(Window, ListInteraction) {
  Window w;

  vector<int> selected;
  vector<string> options {"Wisconsin", "Minnesota", "Iowa"};
  List *state = new List(1, 8, options);
  Label *l = new Label(0, 0, "Select Wisconsin...");
  Button *b = new Button(25, 3, "Quit");
  w.Add(state);
  w.Add(l);
  w.Add(b);
  int nclicks = 0;

  Checkbox *box1 = new Checkbox(5, 20, "animal");
  Checkbox *box2 = new Checkbox(5, 21, "vegetable");
  box2->IsChecked(true);
  Checkbox *box3 = new Checkbox(5, 22, "mineral");
  Slider *s = new Slider(7, 12, 7, 4);

  box1->AddListener([&](bool is) {
    ++nclicks;
    selected.push_back(100 + (is ? 10 : 0));
    if (nclicks == 5) {
      l->SetText("Now uncheck the vegetable checkbox...");
    }
  });
  box2->AddListener([&](bool is) {
    ++nclicks;
    selected.push_back(101 + (is ? 10 : 0));
    if (nclicks == 6) {
      l->SetText("Now check the vegetable checkbox...");
    } else if (nclicks == 7) {
      l->SetText("Now check the mineral checkbox...");
    }
  });
  box3->AddListener([&](bool is) {
    ++nclicks;
    selected.push_back(102 + (is ? 10 : 0));
    if (nclicks == 8) {
      w.Add(s);
      l->SetText("Now set the slider to 0...");
    }
  });

  state->AddListener([&](int i) {
    selected.push_back(i); 
    ++nclicks;
    if (nclicks == 1) {
      l->SetText("Now select Iowa...");
    } else if (nclicks == 2) {
      l->SetText("Now select Minnesota...");
    } else if (nclicks == 3) {
      l->SetText("Now deselect Minnesota...");
    } else if (nclicks == 4) {
      w.Add(box1);
      w.Add(box2);
      w.Add(box3);
      l->SetText("Now check the animal checkbox...");
    }
  });

  s->AddListener([&](int i) {
    ++nclicks;
    selected.push_back(i + 1000);
    if (nclicks == 9) {
      l->SetText("Now set the slider to its maximum value...");
    } else if (nclicks == 10) {
      l->SetText("Now click Quit...");
    }
  });

  b->AddListener([&]() {
    selected.push_back(300);
    w.Stop();
  });

  w.Loop();

  vector<int> expected {0, 2, 1, -1, 110, 101, 111, 112, 1000, 1007, 300};
  ASSERT_EQ(expected, selected) << "I expected a certain sequence of selected items from the ListInteraction test, but I got a different sequence.";
}

class DummyWidget : public Widget {
  public:
    DummyWidget() :
      Widget(0, 0, 10, 10) {
    }

    void Draw() {}

    ~DummyWidget() {
      is_destroyed = true;
    }
    
    static bool is_destroyed;
};

bool DummyWidget::is_destroyed = false;

TEST(Widget, Dtor) {
  Widget *w = new DummyWidget();
  delete w;
  ASSERT_TRUE(DummyWidget::is_destroyed) << "I tried deleting a subclass of Widget through a Widget *, but it's deconstructor wasn't called.";
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv); 
  return RUN_ALL_TESTS();
}
