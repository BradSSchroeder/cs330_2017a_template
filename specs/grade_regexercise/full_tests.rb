#!/usr/bin/env ruby

require 'minitest/autorun'
require 'compare.rb'

class FullTests < MiniTest::Test
  def test_rationalize
    compare 'rationalize', 'lincoln.txt', 0
    compare 'rationalize', 'fractions.csv', 0
    compare 'rationalize', 'weather.txt', 0
    compare 'rationalize', 'pi.txt', 0
  end

  def test_roy
    compare 'roy', 'fanbow.txt', 0
    compare 'roy', 'colors.list', 0
    compare 'roy', 'color_equations.txt', 0
    compare 'roy', 'hertz1.junk', 0
    compare 'roy', 'hertz2.junk', 0
  end

  def test_footlink
    compare 'footlink', 'footlink1.txt', 0
    compare 'footlink', 'websites.email', 0
    compare 'footlink', 'lonely.txt', 0
    compare 'footlink', 'old_sites.txt', 0
  end

  def assert_expand pattern, expected
    actual = expand(pattern)
    if expected != actual
      flunk <<EOF
I tried calling expand on the pattern #{pattern}, but I didn't get the list I expected.
  expected: #{expected}
    actual: #{actual}
EOF
    end

    actual = `cd ../.tmp/to_trash && ../../regexercise/fileach touch '#{pattern}'`
    expected.each do |f|
      assert File.exists?("../.tmp/to_trash/#{f}"), "I tried running 'fileach touch #{pattern}', but it didn't generate the file #{f}."
    end

    actual = `cd ../.tmp/to_trash && ../../regexercise/fileach echo '#{pattern}'`
    assert_equal expected.map {|e| "#{e}\n"}.join, actual, "I tried running 'fileach echo #{pattern}', but it didn't generate the output I expected"

    actual = `cd ../.tmp/to_trash && ../../regexercise/fileach "rm -f" '#{pattern}'`
    expected.each do |f|
      assert !File.exists?("../.tmp/to_trash/#{f}"), "I tried running 'fileach \"rm -f\" #{pattern}', but it didn't remove the file #{f}."
    end
  end

  def test_fileach
    problem = 'fileach'
    assert File.exist?(problem), "I couldn't find a script named #{problem}."
    assert File.executable?(problem), "Script #{problem} wasn't executable."

    load 'fileach'
    assert_expand 'file{A,B,C}', ["fileA", "fileB", "fileC"]
    assert_expand 'b<0-4>', %w{b0 b1 b2 b3 b4}
    assert_expand 'f<9-11>_<3-5>', %w{f9_3 f9_4 f9_5 f10_3 f10_4 f10_5 f11_3 f11_4 f11_5}
    assert_expand 'pic{fall,summer,winter}<16-17>', %w{picfall16 picfall17 picsummer16 picsummer17 picwinter16 picwinter17}
    assert_expand '<10-12>{i,j,k}<7-8><1-2>', %w{10i71 10i72 10i81 10i82 11i71 11i72 11i81 11i82 12i71 12i72 12i81 12i82 10j71 10j72 10j81 10j82 11j71 11j72 11j81 11j82 12j71 12j72 12j81 12j82 10k71 10k72 10k81 10k82 11k71 11k72 11k81 11k82 12k71 12k72 12k81 12k82}
    assert_expand '{8,8,8}<10-10>', %w{810 810 810}
  end
end
