#!/usr/bin/env ruby

generator = Random.new
punctuation = ",.\n!.:"

200.times do
  case generator.rand 100
    when 0..40
      print "#{generator.rand(1000)} THz "
    when 41..79
      print "#{generator.rand(1000)} "
    when 80..99
      print punctuation[generator.rand(punctuation.length)]
  end
end
