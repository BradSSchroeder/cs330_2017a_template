#!/usr/bin/env ruby

CASESDIR = "#{File.dirname(__FILE__)}/cases"
IS_GENERATING = false

def compare problem, input, expected_status
  assert File.exist?(problem), "I couldn't find a script named #{problem}."
  assert File.executable?(problem), "Script #{problem} wasn't executable."

  actual = `./#{problem} #{CASESDIR}/#{input}`
  actual_status = $?.exitstatus
  expected_path = "#{CASESDIR}/#{input.sub(/\.\w+$/, '.expected')}"

  if IS_GENERATING
    File.open(expected_path, 'w') do |f|
      f.write actual
    end
  end

  message = "I tried running #{problem} on cases/#{input}, but I didn't get back the result I expected."

  expected = File.read(expected_path)
  if expected != actual
    puts message
    print "Run vimdiff? (Hit :qa to quit.) [y] or n: "
    response = gets.chomp
    if response !~ /^n/
      actual_path = '../.tmp/to_trash/tmpout'
      File.open(actual_path, 'w') do |f|
        f.write(actual)
      end
      system("vimdiff #{expected_path} #{actual_path}")
    end

    flunk message
  end

  assert_equal expected_status, actual_status, "I tried running #{problem} on cases/#{input}, but I didn't get back the exit status I expected"
end
